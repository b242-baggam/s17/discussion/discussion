console.log("hello world");

// (section) function
/* 
function in javascriipt are lines/blocks of code that tell us our device
to perform a certain task when they are called/invoked

function are mostly created to create complicated tasks to run
several lines of code in succession */

// function declaration
//  defines a function with the specified parameters 

/* syntax:
     function functionName(){
        codes to be executed (statements);
     }
*/

function printName(){
    console.log("Hello again");
}

printName();

// [section] function invocation 
// the code block and the statements inside the function is not 
// immediately exceuted when the function is defined

// the coide will be excecuted when ther fucntion is invoked or called 

// reuse the codes inside 
printName();

// [section] function  declaration vs function expression


// a function ca be created through function declaration through the 
// use of "function" keyword and addding a function name.

// in JS , function can be hoisted , hoisting is the JS manner wherein
// fucntion can be run or used before they are created.
function declaredFunction(){
    console.log("hello from declaredFunction()");
}
declaredFunction();

/*
let ans const variobales cannot be hoisted in JS 
console.log(name);
let name;   */

// function expression
// a function can be stored in a variable 
//  a function expresssion is an anonymous fucntion assigned to a variable
/*
   syntax:
       let/const variableFunction = function(){
        codes to be executed/statements 
       }   */

// variableFunction();   /* ERROR - cuz variable is used before assigning */ 
// but u can call the function name before assigning them, but here the function was used as variable henc error
let variableFunction = function(){
    console.log("hello from the variableFunction()");
}
variableFunction();

let funcExpression = function funcName(){
    console.log("hello from the other side!");
}
// funcName();  
/* OUTPUT => ERROR funcName is not defined */
// we need to call the variable assigned to funcName() then it will not show erro
funcExpression();

// reassign declared fucntion and function expression to new anoonymous functions
declaredFunction = function(){
    console.log("updated declaredFunction()");
}
declaredFunction();

funcExpression = function(){
    console.log('updated funcExpression()');
}
funcExpression();

// we cannot assign a new fucntion expression initialized with const keyword 
const constFunction = function(){
    console.log("const function");
}
constFunction();

// constFunction = function(){
//     console.log("cannot be reassigned!");
// }
// constFunction();

// [Section] Function Scoping
/*
	scope is the accessibility (visibility) of variables

	JS has 3 scopes for its variables
		1. local/block scope
		2. global scope
		3. function scope
            Javascript has a function scope: each function creates a new scope
            only be accessed insode the curly brace whrer it is created 
*/ 
{
	let localVar = "John Doe";
    console.log(localVar);
    let globalVar = "Jane Doe";  /*since its global var , hence it is accesssable here as well */
}
let globalVar = "Jane Doe";

console.log(globalVar);
// console.log(localVar);  /* this return error - cuz local can only be accesed in curly brackets 

function showName(){
    const functionConst = "Joe";
    let functionLet = "John";
    
    console.log(functionConst);
    console.log(functionLet);
}

showName();
// returns an error cuz they are inside the function showNames()
// console.log(functionConst);
// console.log(functionLet);

// nested funtion
function myNewFunction(){
    let name = "Jane";

    function nestedFunction(){
        let nestedVar = "John";
        console.log(name); // will work cuz nestdFunction() is still 
        // inside the function (myNewFunction) where "name" variabke is declared 
    }
    // console.log(nestedVar);   results in error cuz of function scope in Js
    nestedFunction(); // works cuz nestedfunction is called inside the function where it is declared
}
myNewFunction();
// nestedFunction(); // dosnt work cuz nestd function is calleed outside the fn() where it is declared 

let globalName ='Alexandro';
function newFunction2(){
    let nameInside = 'Renz';
    console.log(globalName);
}
newFunction2();
// console.log(nameInside);   results to an error 

// [section] use of alert()
/* alert() allows us to show a small window at the top of ours browser
   page to show information to our users */
alert("hello world");
// we can also use alert() to show a  message to the users from tue later function invocation
function sampleAlert(){
    alert("Hello, user");
} 
sampleAlert();

console.log('I will only log in the console after the alert is closed');

// [section] use of prompt()
/* prompt() alllows us to show a small window at the top of the browser
   page to gather user input. much like alert it will have the page wait
   until the user completes or enters their input.    */

// prompt() can be asssigned to a varible 
/* input from the prompt() will be returned as a string data type 
once the user dismisses the window */

/* prompt returns empty string if the user clickes 'ok' button witout
   entering any input and null if the user cancels the prompt() */
let samplePrompt = prompt("Enter your name");
console.log("hello, " + samplePrompt);
console.log(typeof samplePrompt)  // returns a string data type 

/* miniactivity*/
let firstname = prompt("Enter your firstname");
let lastname = prompt("Enter your lastname");
function printWelcomeMessage(){
    console.log("hello, " + firstname + ' ' + lastname + '! welcome to my page');    
}
printWelcomeMessage();

// sir solution - use let firstname and lastname inside the fn to make them accessible only to funtion 
// function printWelcomeMessage(){
//     let firstname = prompt("Enter your firstname");
//     let lastname = prompt("Enter your lastname");
//     console.log("hello, " + firstname + ' ' + lastname + '! welcome to my page');    
// }
// printWelcomeMessage();


// [section] funtion naming conventions
/*  function nmaes should be definitive of the task it will 
    perfiorm . it is usually starts with a verb*/

function getCourses(){
    let courses = ['science 101', 'math 101', 'english 101'];
    console.log(courses);
}
getCourses();

// avoid generic names to avoid confusion within our codes
function get(){
    let name = 'jamie';
    console.log(name);
}
get();

// avoid pointlesss and inapropriate function names
function foo(){
    console.log(25%5);
}
foo();

// name our fucntion in small cops. follow cameCasing when naming variables and fn with more than 2 words
function displayCarInfo(){
    console.log('Brand: Toyota');
    console.log('Type: Seden');
    console.log('Price: 1 500 000');
}
displayCarInfo();
